package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException{
         try {
            List<Integer> list = new ArrayList<Integer>(inputNumbers);
                //Make function have no side effect
            Collections.sort(list);


            int layers = numberOfLayers(list.size());
            int[][] pyramid = new int[layers][layers * 2 - 1];

            int currentLevel = 0;
            int idAtLevel = 0;
            for (Integer aList : list) {
                if (idAtLevel > currentLevel) {
                    currentLevel++;
                    idAtLevel = 0;
                }
                int placeAtLevel = 2 * idAtLevel - currentLevel + layers - 1;
                pyramid[currentLevel][placeAtLevel] = aList;
                idAtLevel++;
            }
            return pyramid;
        }catch (CannotBuildPyramidException e){
            throw e;
        }catch (Throwable e){
            //Any exceptions like OutOfMemoryError or more unpredictable
            //Should not leave this method
            throw new CannotBuildPyramidException(e);
        }
    }



    private int numberOfLayers(int count){
        //count == layers*(layers+1)/2
        //layers == sqrt(2*count) rounded
        int layers = (int) Math.sqrt(2*count);
        if(count != layers*(layers+1)/2)
            throw new CannotBuildPyramidException(String.format("Incorrect number of count: %d", count));
        return layers;
    }
}
