package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    public CannotBuildPyramidException(){
        super();
    }
    public CannotBuildPyramidException(Throwable reason){
        super(reason);
    }
    public CannotBuildPyramidException(String message){
        super(message);
    }
    public CannotBuildPyramidException(String message, Throwable reason){
        super(message,reason);
    }

}
