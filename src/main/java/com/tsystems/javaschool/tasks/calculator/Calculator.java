package com.tsystems.javaschool.tasks.calculator;


import java.util.Locale;
import java.util.Scanner;

public class Calculator {



    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Calculator calc = new Calculator();
        while (true){
            String s = in.next();
            if(s.equalsIgnoreCase("q")||s.equalsIgnoreCase("quit")){
                break;
            }
            try {
                System.out.println(calc.evaluate(s));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if(statement == null)return null;
        if(!stringConsistsOfAllowedCharacters(statement)) return null;
        if(!stringHasPossibleParenthesesOrder(statement)) return null;
        try{
            return format(calculate(statement));
        }catch (IllegalArgumentException e){
            return null;
        }
    }

    private static String format(double input){
         String usual = Double.toString(input);
         try {
             Locale en = Locale.ENGLISH;
             if(Math.abs(input)<=0.00005) {
                 return String.format("%.4f", 0f);
             }
             int dotIndex = usual.indexOf('.');
             String rest = usual.substring(dotIndex + 1);
             String start = usual.substring(0, dotIndex);
             if (rest.equals("0")) return start;
             if (rest.length() <= 4) return usual;
             if (start != "0") return String.format(en, "%.4f", input);
             //looking for significant digits
             double limit = 1;
             int digitsAfterDot = 0;
             while (limit > input) {
                 digitsAfterDot++;
                 limit /= 10;
             }
             return String.format(en, "%." + digitsAfterDot + "f", input);
         }catch (RuntimeException e){
             return usual;
         }
    }


    private static double calculate(String statement) throws IllegalArgumentException{
        //0)If it`s empty, it can`t be calculated
        if(statement==null||statement.length()==0)
            throw new IllegalArgumentException(statement);
        //1)If it is a number, return its value
        try{
            return Double.parseDouble(statement);
        }catch (IllegalArgumentException e){
            //not a number? it is possible. Ignore this exception
        }
        //2)Remove outer brackets if possible
        if(statement.charAt(0)=='(' &&
           statement.charAt(statement.length()-1)==')'){
            String inner = statement.substring(1, statement.length()-1);
            if(stringHasPossibleParenthesesOrder(inner))
                return calculate(inner);
            //else it means that the first and the last brackets are in different pairs
        }

        //+ and - have lower priority of calculation if not in parentheses

        //Now we split statement to two parts at '+' or '-' to calculate them separately
        try{
            return useOperators(statement,'+', '-');
        }catch (ArithmeticException e){
            throw new IllegalArgumentException(e);
        }catch (NoOperatorsFoundException e2){
            //ignore
        }

        //now we know that statement is not a raw number
        //it is not included into parentheses
        //it has no +- in inception = 0 level
        //it means that statement consists of multiplied or divided statements
        try{
            return useOperators(statement,'*', '/');
        }catch (ArithmeticException e){
            throw new IllegalArgumentException(e);
        }catch (NoOperatorsFoundException e2){
            //ignore
        }

        //Not a number, not a parenthese, not a sum, not a product...
        //What the hell are you?
        throw new IllegalArgumentException(statement);
    }

    private static double useOperators(String statement, char operator1, char operator2) throws NoOperatorsFoundException{
        int inception = 0;
        //going backwards, because forward causes mistake
        // 10/2-7+3*4 != 10/2-(7+3*4)
        for (int i = statement.length()-1; i>=0; i--) {
            char c = statement.charAt(i);
            if(c == '(') inception++;
            if(c == ')') inception--;
            //we don`t split statement inside parentheses
            if(inception == 0 && (c==operator1||c==operator2)){
                String left = statement.substring(0, i);
                String right = statement.substring(i+1, statement.length());
                double leftDouble = calculate(left);
                double rightDouble = calculate(right);
                return operate(leftDouble,c,rightDouble);
            }
        }
        throw new NoOperatorsFoundException();
    }

    private static double operate(double left, char operator, double right){
        if(operator == '+')return left+right;
        if(operator == '-')return left-right;
        if(operator == '*')return left*right;
        //if we divide by zero, java returns infinity
        //but we need exception
        if(right==0)throw new IllegalArgumentException("Dividing by " + Double.toString(right));
        if(operator == '/')return left/right;
        throw new IllegalArgumentException("Unknown operator: " + Character.toString(operator));
    }

    private static boolean stringHasPossibleParenthesesOrder(String statement){
        int inception = 0;
        for (int i = 0; i < statement.length(); i++) {
            if(statement.charAt(i) == '(') inception++;
            if(statement.charAt(i) == ')') inception--;

            if(inception<0)return false;
        }
        if(inception!=0)return false;
        return true;
    }
    private static boolean stringConsistsOfAllowedCharacters(String statement){
        for (int i = 0; i < statement.length(); i++) {
            if(!isCharacterAllowed(statement.charAt(i))){
                return false;
            }
        }
        return true;
    }
    private static boolean isCharacterAllowed(char c){
        if(c>='0'&&c<='9') return true;
        if(c=='+'||c=='-'||c=='*'||c=='/')return true;
        if(c=='('||c==')')return true;
        if(c=='.')return true;
        return false;
    }

    private static class NoOperatorsFoundException extends RuntimeException{}

}
