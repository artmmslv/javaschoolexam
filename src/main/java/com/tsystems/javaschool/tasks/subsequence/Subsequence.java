package com.tsystems.javaschool.tasks.subsequence;


import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(x == null || y == null)
            throw new IllegalArgumentException();
        int xid = 0;
        int yid = 0;
        while(xid<x.size()){
            if(yid == y.size())
                return false;
            if(equal(x.get(xid), y.get(yid))) {
                xid++;
            }
            yid++;
        }
        return true;
    }

    private boolean equal(Object x, Object y){
        if(x == null && y == null)
            return true;
        if(x == null || y == null)
            return false;
        return x.equals(y);
    }
}
